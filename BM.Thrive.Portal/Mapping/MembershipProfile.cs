﻿using AutoMapper;
using BM.Thrive.Portal.Models;
using BMThrive.Common.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BM.Thrive.Portal.Mapping
{
    public class MembershipProfile : Profile
    {
        public MembershipProfile()
        {
            CreateMap<MemberAddress, AddressDto>();
            CreateMap<AddressDto, MemberAddress>();

            CreateMap<Member, MemberDto>();
            CreateMap<MemberDto, Member>();
        }
    }
}
