﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BM.Thrive.Portal.Models
{
    public class AuthResponse
    {
        public string access_token;
        public string token_type;
        public string expires_in;
        public string userName;
    }
}
