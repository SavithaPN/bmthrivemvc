﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using BMThrive.Common.Dto;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace BM.Thrive.Portal.Models
{
    public class Member
    {
        public Guid Id { get; set; }

        [Required(ErrorMessage = "ದಯವಿಟ್ಟು ಹೆಸರನ್ನು ಭರ್ತಿ ಮಾಡಿ")]
        public string Name { get; set; }
        public MemberAddress Address { get; set; }
        public string IdInfoPrimary { get; set; }
        public string IdInfoSecondry { get; set; }
        public string IdInfoAddtional { get; set; }
        [Required(ErrorMessage = "ದಯವಿಟ್ಟು ಫೋನ್ ಸಂಖ್ಯೆಯನ್ನು ಭರ್ತಿ ಮಾಡಿ")]
        public string ContactNumber { get; set; }
        [Required(ErrorMessage = "ದಯವಿಟ್ಟು ನಿಮ್ಮ ಇಮೇಲ್ ಅನ್ನು ನಮೂದಿಸಿ")]
        public string Email { get; set; }
        public string AdditionalInfo { get; set; }
        [Required(ErrorMessage = "ದಯವಿಟ್ಟು ಸದಸ್ಯತ್ವ ಪ್ರಕಾರವನ್ನು ಆಯ್ಕೆಮಾಡಿ")]
        public Guid MemberType_Id { get; set; }
        public DateTime RegisteredDate { get; set; }
        public string Status { get; set; }
        public bool IsActive { get; set; }
        public Guid CustomerId { get; set; }

        public string PhotoPath { get; set; }

        public MemberMediaDto MemberPhoto { get; set; }
        public IFormFile PhotoUpload { get; set; }

        public List<SelectListItem> MemberTypes { get; set; } = new List<SelectListItem>();

        public int selectedTab { get; set; }
    }
}
