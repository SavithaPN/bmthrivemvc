﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BM.Thrive.Portal.Models
{
    public class MemberAddress
    {
        public Guid Id { get; set; }
        public Guid MemberId { get; set; }
        [Required(ErrorMessage = "ದಯವಿಟ್ಟು ವಿಳಾಸವನ್ನು ಭರ್ತಿ ಮಾಡಿ")]
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string State { get; set; }
        [Required(ErrorMessage = "ದಯವಿಟ್ಟು ನಗರವನ್ನು ಭರ್ತಿ ಮಾಡಿ")]
        public string District { get; set; }
        public string WardNumber { get; set; }
        public string City { get; set; }
        [Required(ErrorMessage = "ದಯವಿಟ್ಟು ಪಿನ್ಕೋಡ್ ಅನ್ನು ಭರ್ತಿ ಮಾಡಿ")]
        public string ZipCode { get; set; }
        public string Country { get; set; }
    }
}
