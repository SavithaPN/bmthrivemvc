﻿
using BM.Thrive.Portal.Models;
using BM.Thrive.Portal.Services;
using BMThrive.Common.Dto;
using BMThrive.Common.Dto.Enum;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace BM.Thrive.Portal.Controllers
{
    public class MembershipController : Controller
    {
        private readonly MembershipService _membershipService;
        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly IConfiguration _configuration;
        private readonly Guid _customerId;
        private readonly string _baseAddress;
        public MembershipController(MembershipService MembershipService,
            IWebHostEnvironment hostingEnvironment,
            IConfiguration configuration)
        {
            _membershipService = MembershipService;
            _hostingEnvironment = hostingEnvironment;
            _configuration = configuration;
            _customerId = Guid.Parse(configuration["SiteServices:CustomerId"]);
            _baseAddress = configuration["SiteServices:BaseAddress"];

        }
        [HttpGet]
        public IActionResult Index()
        {
            Member member = new Member();
            return View(member);
        }

        [HttpPost]
        public async Task<IActionResult> Index(Member member)
        {
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                return View();
            }

            member.Id = Guid.NewGuid();
            string uniqueFileName = null;
            if (member.PhotoUpload != null)
            {
                string uploadFolder = Path.Combine(_hostingEnvironment.WebRootPath, "images/ProfilePics");
                uniqueFileName = Guid.NewGuid().ToString() + "_" + member.PhotoUpload.FileName;
                string filePath = Path.Combine(uploadFolder, uniqueFileName);
                member.PhotoUpload.CopyTo(new FileStream(filePath, FileMode.Create));
                member.PhotoPath = Path.Combine(uploadFolder, uniqueFileName);

                member.MemberPhoto = new MemberMediaDto();

                using (var memoryStream = new MemoryStream())
                {
                    await member.PhotoUpload.CopyToAsync(memoryStream);
                    member.MemberPhoto.MediaFile = memoryStream.ToArray();
                    string mediaFile = @"/api/Membership/DownloadProfilePic?memberId=" + member.Id;
                    member.MemberPhoto.MediaFileDownloadUrl = _baseAddress + mediaFile;
                    member.MemberPhoto.MediaContentType = member.PhotoUpload.ContentType;
                    member.MemberPhoto.Active = true;
                }
            }

            member.CustomerId = _customerId;
            WorkResultDto<Guid, GeneralWorkStatus> oWorkResult = await _membershipService.RegisterMember(member);

            if (oWorkResult.Status == GeneralWorkStatus.Success)
            {
                ViewData["Status"] = "Success";
                if(member.selectedTab > 0)
                {
                    int rate = member.selectedTab;
                    //Create a order here
                    var orderModel = CreateOrderModel(member, rate);
                    return View("RazorPaymentPage", orderModel);
                }
               

            }

            return View();
        }

        private OrderModel CreateOrderModel(Member member, int amount)
        {
            Random randomObj = new Random();
            string transactionId = randomObj.Next(10000000, 100000000).ToString();

            Razorpay.Api.RazorpayClient client = new Razorpay.Api.RazorpayClient("rzp_test_wPW00e3AoF38gh", "hc2r3MaMLof8afMOGxqD25ZV");
            Dictionary<string, object> options = new Dictionary<string, object>();
            options.Add("amount", amount * 100);  // Amount will in paise
            options.Add("receipt", transactionId);
            options.Add("currency", "INR");
            options.Add("payment_capture", "0"); // 1 - automatic  , 0 - manual
                                                 //options.Add("notes", "-- You can put any notes here --");
            Razorpay.Api.Order orderResponse = client.Order.Create(options);
            string orderId = orderResponse["id"].ToString();

            // Create order model for return on view
            OrderModel orderModel = new OrderModel
            {
                orderId = orderResponse.Attributes["id"],
                razorpayKey = "rzp_test_wPW00e3AoF38gh",
                amount = amount *100,
                currency = "INR",
                name = member.Name,
                email = member.Email,
                contactNumber = member.ContactNumber,
                address = member.Address.Address1,
                description = "Testing description"
            };

            // Return on PaymentPage with Order data
            return orderModel;
        }

        //[HttpPost]
        //public async Task<IActionResult> Payment(Member member)
        //{
        //    return View();
        //}
    }
}
