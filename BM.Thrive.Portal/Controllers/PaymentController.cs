﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using Microsoft.Extensions.Primitives;

using System.Linq;
using System.Threading.Tasks;
using BM.Thrive.Portal.Models;
using Razorpay.Api;
using Razorpay.Api.Errors;

namespace BM.Thrive.Portal.Controllers
{
    public class PaymentController : Controller
    {
        // GET: Payment
        public ActionResult Index()
        {
            return View();
        }


        [HttpPost]
        public ActionResult CreateOrder(Models.PaymentInitiateModel _requestData)
        {
            // Generate random receipt number for order
            Random randomObj = new Random();
            string transactionId = randomObj.Next(10000000, 100000000).ToString();

            Razorpay.Api.RazorpayClient client = new Razorpay.Api.RazorpayClient("rzp_test_wPW00e3AoF38gh", "hc2r3MaMLof8afMOGxqD25ZV");
            Dictionary<string, object> options = new Dictionary<string, object>();
            options.Add("amount", _requestData.amount * 100);  // Amount will in paise
            options.Add("receipt", transactionId);
            options.Add("currency", "INR");
            options.Add("payment_capture", "0"); // 1 - automatic  , 0 - manual
                                                 //options.Add("notes", "-- You can put any notes here --");
            Razorpay.Api.Order orderResponse = client.Order.Create(options);
            string orderId = orderResponse["id"].ToString();

            // Create order model for return on view
            OrderModel orderModel = new OrderModel
            {
                orderId = orderResponse.Attributes["id"],
                razorpayKey = "rzp_test_wPW00e3AoF38gh",
                amount = _requestData.amount * 100,
                currency = "INR",
                name = _requestData.name,
                email = _requestData.email,
                contactNumber = _requestData.contactNumber,
                address = _requestData.address,
                description = "Testing description"
            };

            // Return on PaymentPage with Order data
            return View("PaymentPage", orderModel);
        }

       

        [HttpPost]
        public ActionResult Complete()
        {

            bool IsValidRequest = true;
            Razorpay.Api.Payment paymentCaptured = null;
            bool IsPaymentSuccess;
            bool IsPaymentCapturedSuccess = false;
            // Payment data comes in url so we have to get it from url

            try
            {
                // This id is razorpay unique payment id which can be use to get the payment details from razorpay server
                StringValues paymentId;
                bool res = Request.Form.TryGetValue("rzp_paymentid", out paymentId);

                // This is orderId
                StringValues orderId;
                res = Request.Form.TryGetValue("rzp_orderid", out orderId);

                //This is signature
                StringValues signature;
                res = Request.Form.TryGetValue("rzp_signature", out signature);
                var payload = orderId + '|' + paymentId;
                Utils.verifyWebhookSignature(payload, signature, "hc2r3MaMLof8afMOGxqD25ZV");
                 IsPaymentSuccess = true;
                Razorpay.Api.RazorpayClient client = new Razorpay.Api.RazorpayClient("rzp_test_wPW00e3AoF38gh", "hc2r3MaMLof8afMOGxqD25ZV");
                Razorpay.Api.Payment payment = client.Payment.Fetch(paymentId);

                // This code is for capture the payment 
                Dictionary<string, object> options = new Dictionary<string, object>();
                options.Add("amount", payment.Attributes["amount"]);
                paymentCaptured = payment.Capture(options);
                string amt = paymentCaptured.Attributes["amount"];
                
                string capturedPaymentId = Convert.ToString(paymentCaptured["id"]);
                if (!string.IsNullOrEmpty(capturedPaymentId))
                {
                    IsPaymentCapturedSuccess = true;
                }
                //ViewData["IsValidRequest"] = IsValidRequest;
                //ViewData["IsPaymentSuccess"] = IsPaymentSuccess;
                //ViewData["IsPaymentCapturedSuccess"] = IsPaymentCapturedSuccess;
                ViewBag.IsValidRequest = IsValidRequest;
                ViewBag.IsPaymentSuccess = IsPaymentSuccess;
                ViewBag.IsPaymentCapturedSuccess = IsPaymentCapturedSuccess;
            }
            catch (Razorpay.Api.Errors.BadRequestError ex)
            {
                Console.WriteLine(ex.Message, ex.InnerException);
                IsValidRequest = false;
            }
            catch (SignatureVerificationError ex)
            {
                Console.WriteLine(ex.Message, ex.InnerException);
                IsPaymentSuccess = false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message, ex.InnerException);
            }

            //// Check payment made successfully
            if (paymentCaptured.Attributes["status"] == "captured")
            {
                // Create these action method
                return View("Success");
            }
            else
            {
                return RedirectToAction("Failed");
            }
        }

        public ActionResult Success()
        {
            return View();
        }

        public ActionResult Failed()
        {
            return View();
        }
    }
}
