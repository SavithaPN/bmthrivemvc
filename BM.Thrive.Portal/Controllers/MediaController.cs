﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BM.Thrive.Portal.Controllers
{
    public class MediaController : Controller
    {
        public IActionResult Photos()
        {
            return View();
        }

        public IActionResult Videos()
        {
            return View();
        }
    }
}
