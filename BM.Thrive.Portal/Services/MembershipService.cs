﻿using AutoMapper;
using BM.Thrive.Portal.Models;
using BM.Thrive.Portal.Services.Utilities;
using BMThrive.Common.Dto;
using BMThrive.Common.Dto.Enum;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace BM.Thrive.Portal.Services
{
    public class MembershipService
    {
        private EnhancedRoute _route;
        private readonly HttpClient _httpClient;
        private string _token;
        private Guid _customerId;
        private readonly IMapper _mapper;
        private IConfiguration _configuration;

        public MembershipService(
            HttpClient httpClient,
            IConfiguration configuration,
            IMapper mapper)
        {
            _httpClient = httpClient;
            _route = new EnhancedRoute(httpClient, configuration, "MembershipService", true);
            _customerId = Guid.Parse(configuration["SiteServices:CustomerId"]);
            _mapper = mapper;
            _configuration = configuration;
        }

        public async Task<IEnumerable<MemberDto>> GetMembers()
        {
            var response = await _httpClient.GetAsync(_route.Route);
            response.EnsureSuccessStatusCode();

            var products = await response.Content.ReadAsAsync<IEnumerable<MemberDto>>();

            return products;
        }

        public async Task<MemberDto> GetMemberById(int memberId)
        {
            var response = await _httpClient.GetAsync($"{_route}/{memberId}");
            response.EnsureSuccessStatusCode();

            var product = await response.Content.ReadAsAsync<MemberDto>();

            return product;
        }

        public async Task UpdateMember(MemberDto member)
        {
            await _httpClient.PutAsJsonAsync<MemberDto>($"{_route}/{member.Id}", member);
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> RegisterMember(Member member)
        {
            MemberDto oMDto = _mapper.Map<MemberDto>(member);
            oMDto.Status = "New";
            oMDto.RegisteredDate = DateTime.UtcNow;
            oMDto.MemberType_Id = member.MemberType_Id;
            oMDto.CustomerId = _customerId;
            //var response = await _httpClient.PostAsJsonAsync<MemberDto>(_route.Route + "/RegisterMember", oMDto);
            var response = await _httpClient.PostAsJsonAsync<MemberDto>(_httpClient.BaseAddress + "/api/Membership/RegisterMember", oMDto);
            WorkResultDto<Guid, GeneralWorkStatus> oWorkResult = await response.Content.ReadAsAsync<WorkResultDto<Guid, GeneralWorkStatus>>();

            return oWorkResult;

        }

        public async Task<PagedListViewModel<Member>> ListMembers()
        {

            var response = await _httpClient.GetAsync(_httpClient.BaseAddress + "/api/Membership/ListMembersSorted?customerId=" + _customerId);
            response.EnsureSuccessStatusCode();
            PagedListViewModel<MemberDto> members = await response.Content.ReadAsAsync<PagedListViewModel<MemberDto>>();

            PagedListViewModel<Member> oMemberList = new PagedListViewModel<Member>();
            oMemberList.PageItems = new List<Member>();
            oMemberList.TotalItemCount = members.TotalItemCount;

            foreach (var oMember in members.PageItems)
            {
                Member oMemberNew = _mapper.Map<Member>(oMember);
                oMemberList.PageItems.Add(oMemberNew);
            }

            return oMemberList;

        }

        public async Task<PagedListViewModel<Member>> SearchListMembers(string searchText = "")
        {
            UriBuilder uriBuilder = new UriBuilder(_httpClient.BaseAddress + "/api/Membership/ListMembersSorted");
            var queryParameter = HttpUtility.ParseQueryString(uriBuilder.Query);
            queryParameter["customerId"] = _customerId.ToString();
            queryParameter["search"] = searchText;
            uriBuilder.Query = queryParameter.ToString();
            var response = await _httpClient.GetAsync(uriBuilder.ToString());

            response.EnsureSuccessStatusCode();
            PagedListViewModel<MemberDto> members = await response.Content.ReadAsAsync<PagedListViewModel<MemberDto>>();

            PagedListViewModel<Member> oMemberList = new PagedListViewModel<Member>();
            oMemberList.PageItems = new List<Member>();
            oMemberList.TotalItemCount = members.TotalItemCount;

            foreach (var oMember in members.PageItems)
            {
                Member oMemberNew = _mapper.Map<Member>(oMember);
                oMemberList.PageItems.Add(oMemberNew);
            }

            return oMemberList;
        }

        public async Task<List<MemberTypeDto>> GetMemberTypes()
        {
            var response = await _httpClient.GetAsync(_httpClient.BaseAddress + "/api/Membership/ListMemberTypes");
            response.EnsureSuccessStatusCode();
            List<MemberTypeDto> memberTypes = await response.Content.ReadAsAsync<List<MemberTypeDto>>();
            return memberTypes;
        }
    }
}
