﻿using BM.Thrive.Portal.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace BM.Thrive.Portal.Services.Utilities
{
    public class EnhancedRoute
    {
        private readonly HttpClient _httpClient;
        private string _token;
        private readonly string _route;

        public EnhancedRoute(HttpClient httpClient,
           IConfiguration configuration,
           string serviceName,
           bool bAuhtendicated = false)
        {
            _route = configuration[serviceName + ":ControllerRoute"];
            _httpClient = httpClient;

            GetToken().Wait();

        }

        public async Task GetToken()
        {
            RestParameters oParameters = new RestParameters();
            oParameters.AddParameter("grant_type", "password");
            oParameters.AddParameter("username", "admin");
            oParameters.AddParameter("password", "admin123");

            var response = await _httpClient.PostAsync("/api/Token", oParameters.GetHttpContent);
            response.EnsureSuccessStatusCode();

            var authresponse = await response.Content.ReadAsAsync<AuthResponse>();

            _token = authresponse.access_token;

        }

        public string Route
        {
            get
            {
                return _route + "?api_key=" + _token;
            }
        }
    }
}
