﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace BM.Thrive.Portal.Services.Utilities
{
    public class RestParameters
    {
        private Dictionary<string, string> _parameters = new Dictionary<string, string>();
        private readonly HttpClient _httpClient;
        private string _token;
        private readonly string _route;

        public RestParameters()
        {

        }

        public void AddParameter(string sName, string sValue)
        {
            _parameters.Add(sName, sValue);
        }

        public void Clear()
        {
            _parameters.Clear();
        }

        public Dictionary<string, string> GetRaw()
        {
            return _parameters;
        }

        public HttpContent GetHttpContent
        {
            get
            {
                var encodedContent = new FormUrlEncodedContent(_parameters);
                return encodedContent;
            }

        }
    }
}
